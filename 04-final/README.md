# Complete kustomize example

## Description

Basic k8s deployment / service / ingress in three different environments *dev* / *qa* / *prod*

Each environment (overlay) customizes:

* ref label (with environment name as label)
* deployment and service selectors (ref tag)
* deployment container image:
  * 'latest' for *dev* environment
  * 'qa' for *qa* environment
  * 'stable' for *prod* environment
* ingress host name (ingress.yaml).
* application config using ConfigMaps (deployment.yaml)
* application secrets using Secrets (deployment.yaml)
* ConfigMaps values loaded from environment files
* secret for database password comes from DB_PASSWORD environment variable
* using COMMIT_SHA variable value as 'version' label (deployment.yaml)
* value for COMMIT_SHA variable comes from COMMIT_SHA environment variable

## Running kustomize

Inside `04-final` directory run:

```
DB_PASSWORD=superSecretPassword COMMIT_SHA=de231f kubectl kustomize overlays/dev
```

to see manifests for *dev* environment

```
DB_PASSWORD=superSecretPassword COMMIT_SHA=4321fe kubectl kustomize overlays/qa
```

to see manifests for *qa* environment


```
DB_PASSWORD=superSecretPassword COMMIT_SHA=123f32 kubectl kustomize overlays/prod
```

to see manifests for *prod* environment


To apply manifest for *dev* environment run:

```
DB_PASSWORD=superSecretPassword COMMIT_SHA=de231f kubectl apply -k overlays/dev
```


