# Basic kustomize example

## Description

Basic k8s deployment / service in three different environments *dev* / *qa* / *prod*

Each environment customizes:

* ref label (with environment name as label)
* service selector (ref tag)
* deployment container image:
  * 'latest' for *dev* environment
  * 'qa' for *qa* environment
  * 'stable' for *prod* environment

## Running kustomize

Inside `03-app_secret` directory run:

```
kubectl build overlays/dev
```

to see manifests for *dev* environment

```
kubectl build overlays/qa
```

to see manifests for *qa* environment


```
kubectl build overlays/prod
```

to see manifests for *prod* environment


To apply manifest for *dev* environment run:

```
kubectl apply -k overlays/dev
```


